//
//  LIstRowView.swift
//  IosTemplateApp
//
//  Created by Onur on 27.08.2022.
//

import SwiftUI

struct LIstRowView: View {
    var item : ItemModel = itemArray[0]
    var body: some View {
        HStack (alignment: .top, spacing: 10){
            Circle()
                .stroke()
                .frame(width: 33, height: 33)
                //.background(item.gradiend)
                .overlay(Image(item.image).resizable().frame(width: 20, height: 20, alignment: .center))
            VStack (alignment: .leading, spacing: 5){
                Text(item.title)
                    .fontWeight(.bold)
                Text(item.subtitle)
                    .font(.subheadline)
            }
        }
    }
}

struct LIstRowView_Previews: PreviewProvider {
    static var previews: some View {
        LIstRowView()
    }
}
