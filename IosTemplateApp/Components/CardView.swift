//
//  CardView.swift
//  IosTemplateApp
//
//  Created by Onur on 27.08.2022.
//

import SwiftUI

struct CardView: View {
    // ilk yontem
    var item : ItemModel
    //ikinci yontem asagida ONEMLI!!!
    //var item : ItemModel = itemArray[0]
    var body: some View {
        VStack (alignment: .leading, spacing: 8){
            HStack {
                Spacer()
                Image(item.image)
                    .resizable()
                    .frame(width: 100, height: 100, alignment: .center)
                Spacer()
            }
            Text(item.title)
                .font(.title)
                .fontWeight(.bold)
            Text(item.subtitle)
                .opacity(0.7)
                .lineLimit(2)
            Text("20 seconds to 3 hours")
                .opacity(0.7)
        }
        .foregroundColor(Color.white)
        .padding(16)
        .frame(width: 270, height: 330)
        .background(item.gradiend)
        .cornerRadius(20)
    }
}

struct CardView_Previews: PreviewProvider {
    static var previews: some View {
        // bir yontem asagidaki gibi gorunum icinde dizenin ilk elemaninin secmek -- digeri icin yukaridaki youma bak
        CardView(item: itemArray.first!)
    }
}
