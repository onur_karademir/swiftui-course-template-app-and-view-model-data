//
//  SmallCardView.swift
//  IosTemplateApp
//
//  Created by Onur on 27.08.2022.
//

import SwiftUI

struct SmallCardView: View {
    var item : ItemModel = itemArray[0]
    var body: some View {
        VStack (alignment: .leading, spacing: 8){
            HStack {
                Spacer()
                Image(item.image)
                    .resizable()
                    .frame(width: 100, height: 100, alignment: .center)
                Spacer()
            }
            Text(item.title)
                .font(.title)
                .fontWeight(.bold)
            Text(item.subtitle)
                .opacity(0.7)
                .lineLimit(2)
            Text("20 seconds to 3 hours")
                .opacity(0.7)
        }
        .foregroundColor(Color.white)
        .padding(16)
        .frame(height: 222)
        .background(item.gradiend)
        .cornerRadius(20)
    }
}

struct SmallCardView_Previews: PreviewProvider {
    static var previews: some View {
        SmallCardView()
    }
}
