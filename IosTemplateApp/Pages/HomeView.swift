//
//  HomeView.swift
//  IosTemplateApp
//
//  Created by Onur on 27.08.2022.
//

import SwiftUI

struct HomeView: View {
    var body: some View {
        NavigationView {
            ScrollView {
                ScrollView  (.horizontal, showsIndicators: false){
                    HStack (spacing: 16){
                        ForEach(itemArray) { item in
                            NavigationLink {
                                DetailView(items: item)
                            } label: {
                                CardView(item: item)
                            }

                        }
                    }
                }
                .padding()
            .navigationTitle("Learn SwiftUI")
                Text("Recent Course")
                    .font(.subheadline).bold()
                    .frame(maxWidth: .infinity, alignment: .leading)
                    .padding()
                
                LazyVGrid(columns: [GridItem(.adaptive(minimum: 160))]) {
                    ForEach(itemArray) { item in
                        SmallCardView(item: item)
                    }
                }
                .padding()
            }
        }
    }
}

struct HomeView_Previews: PreviewProvider {
    static var previews: some View {
        HomeView()
    }
}
