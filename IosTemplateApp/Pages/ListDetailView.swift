//
//  ListDetailView.swift
//  IosTemplateApp
//
//  Created by Onur on 28.08.2022.
//

import SwiftUI

struct ListDetailView: View {
    var items : ItemModel = itemArray[0]
    var body: some View {
            ScrollView {
                VStack {
                    Image(items.image)
                        .resizable()
                        .frame(width: 100, height: 100, alignment: .center)
                    Text(items.title)
                    Text(items.subtitle)
                }
            }
    }
}

struct ListDetailView_Previews: PreviewProvider {
    static var previews: some View {
        ListDetailView()
    }
}
