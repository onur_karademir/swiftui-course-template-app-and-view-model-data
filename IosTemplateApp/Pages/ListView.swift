//
//  ListView.swift
//  IosTemplateApp
//
//  Created by Onur on 27.08.2022.
//

import SwiftUI

struct ListView: View {
    @State var isOpenSheet = false
    var body: some View {
        NavigationView {
                List {
                    ForEach(itemArray) { item in
                        // yeni deneme
                        NavigationLink {
                            ListDetailView(items: item)
                        } label: {
                            LIstRowView(item: item)
                        }
// eski yerlesim asagida
//                        LIstRowView(item: item)
//                            .sheet(isPresented: $isOpenSheet) {
//                                DetailView(items: item)
//                            }
//                            .onTapGesture {
//                                isOpenSheet.toggle()
//                            }
                    }
                }
                .listStyle(.inset)
            .navigationTitle("Course")
        }
    }
}

struct ListView_Previews: PreviewProvider {
    static var previews: some View {
        ListView()
    }
}
