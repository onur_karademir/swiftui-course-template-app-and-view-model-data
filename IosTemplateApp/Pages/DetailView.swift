//
//  DetailView.swift
//  IosTemplateApp
//
//  Created by Onur on 27.08.2022.
//

import SwiftUI

struct DetailView: View {
    var items : ItemModel = itemArray[0]
    var body: some View {
        ScrollView {
            VStack (alignment: .leading, spacing: 8){
                HStack {
                    Spacer()
                    Image(items.image)
                        .resizable()
                        .frame(width: 100, height: 100, alignment: .center)
                    Spacer()
                }
                Text(items.title)
                    .font(.title)
                    .fontWeight(.bold)
                Text(items.subtitle)
                    .opacity(0.7)
                    .lineLimit(2)
                Text("20 seconds to 3 hours")
                    .opacity(0.7)
            }
            .foregroundColor(Color.white)
            .padding(16)
            //.frame(width: 270, height: 330)
            .background(items.gradiend)
        //.cornerRadius(20)
            VStack (alignment: .leading, spacing: 20){
                Text("SwiftUI for IOS 14")
                    .font(.title)
                .fontWeight(.bold)
                Text("Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make.")
                    .font(.subheadline)
            }
            
        }
    }
}

struct DetailView_Previews: PreviewProvider {
    static var previews: some View {
        DetailView()
    }
}
