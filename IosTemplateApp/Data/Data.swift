//
//  Data.swift
//  IosTemplateApp
//
//  Created by Onur on 27.08.2022.
//

import Foundation
import SwiftUI


struct ItemModel : Identifiable {
    var id = UUID()
    var title : String
    var subtitle : String
    var gradiend : LinearGradient
    var image : String
}


let itemArray = [
    ItemModel(title: "Item one title", subtitle: "1 - Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make.", gradiend: LinearGradient(colors: [.blue, .pink],startPoint:.topTrailing,endPoint:.bottomLeading), image: "dove"),
    
    ItemModel(title: "Item two title", subtitle: "2 - Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make.", gradiend: LinearGradient(colors: [.yellow, .blue],startPoint:.topTrailing,endPoint:.bottomLeading), image: "bird"),
    
    ItemModel(title: "Item three title", subtitle: "3 - Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make.", gradiend: LinearGradient(colors: [.red, .orange],startPoint:.topTrailing,endPoint:.bottomLeading), image: "dove"),
    
    ItemModel(title: "Item four title", subtitle: "4 - Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make.", gradiend: LinearGradient(colors: [.black, .gray],startPoint:.topTrailing,endPoint:.bottomLeading), image: "bird")
]
