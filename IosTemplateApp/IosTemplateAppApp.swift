//
//  IosTemplateAppApp.swift
//  IosTemplateApp
//
//  Created by Onur on 27.08.2022.
//

import SwiftUI

@main
struct IosTemplateAppApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
